<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap 5 CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
        .image-login {
            /* Test */
            /* background-image: url(../../../public/img/image-login.jpg); */
            /* ----------------------------------------------------------- */

            background-image: url('img/image-login.jpg');
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            border-top-left-radius: 25px;
            right: 0;
        }

        @media (max-width: 767px) {
            .image-login {
                display: none;
            }
        }
    </style>

    <title>

        @yield('title')

    </title>
</head>

<body>
    <!-- Container -->
    <div class="w-100">
        <div class="d-flex">
            <!-- Left / Content -->
            <div class="col-12 col-md-7 d-flex justify-content-center align-items-center bg-white position-absolute top-0 bottom-0 z-3"
                style="left: 0;">

                @yield('container')

            </div>
            <!-- Left / Content End -->

            <!-- Right / Background Image -->
            <div class="image-login col-12 col-md-5 position-fixed top-0 bottom-0"></div>
            <!-- Right / Background Image End -->
        </div>
    </div>
    <!-- Container End -->

    <!-- Bootstrap 5 JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>