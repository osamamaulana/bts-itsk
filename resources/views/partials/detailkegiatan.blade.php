<style>
    .btn-outline {
        border: 1px solid #ced4da;
        padding: 0.375rem 0.75rem;
        border-radius: 0.25rem;
    }
</style>

<section id="detailkegiatan">
    <div class="card container">
        <div class="card-header bg-white mt-3">
            <h5 class="card-title">Detail Kegiatan</h5>
        </div>
        <div class="card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item p-3">Nama Ketua: {{ $kegiatan->users()->where('jabatan', 'Ketua')->first()->nama }}</li>
                <li class="list-group-item p-3">Nama Anggota:
                    <ul>
                        @foreach ($kegiatan->users()->where('jabatan', 'Anggota')->get() as $anggota)
                            <li>{{ $anggota->nama }}</li>
                        @endforeach
                    </ul>
                </li>
                <li class="list-group-item p-3">Nama Dosen Pembimbing:{{ $kegiatan->users()->where('jabatan', 'Dosen')->first()->nama }} </li>
                <li class="list-group-item p-3">Nama Sekolah: {{ $kegiatan->sekolah }}</li>
                <li class="list-group-item p-3">Tanggal Pelaksanaan: {{ $kegiatan->tanggal_kegiatan }}</li>
                <li class="list-group-item p-3">Status Laporan: @foreach ($laporans as $laporan)
                        <p>{{ $laporan->status_promosi }}</p>
                    @endforeach
                </li>
            </ul>
        </div>
        <div class="card-header bg-white mt-3">
            <h5 class="card-title">File Pendukung</h5>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-primary">
                    <thead>
                        <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Nama File</th>
                            <th scope="col">Jenis</th>
                            <th scope="col">Unduh</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($filePendukung as $file )
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $file->nama }}</td>
                                <td>{{ $file->jenis }}</td>
                                <td><a href="{{ $file->url }}" target="_blank" class="badge bg-dark text-decoration-none"><i data-feather="download"></i> Download</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">Tidak ada file pendukung</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>

        </div>
        <div class="card-header bg-white mt-3">
            <h5 class="card-title">Sertifikat</h5>
        </div>
        <div class="card-body">
            <a href="{{ route('buat', ['id' => $kegiatan->id]) }}" class="btn btn-info detail-button">Download</a>
        </div>
        <div class="card-header bg-white mt-3">
            <h5 class="card-title">Laporan Kegiatan</h5>
        </div>
        <div class="card-body">
            <button type="button" class="badge bg-success text-decoration-none border-0"  data-bs-toggle="modal" data-bs-target="#tambahlaporan"><i data-feather="plus"></i>
                Tambah Laporan
            </button>
            <button type="button" class="badge bg-success text-decoration-none border-0"  data-bs-toggle="modal" data-bs-target="#editlaporan"><i data-feather="edit"></i>
                Edit Laporan
            </button>

            {{-- <badge type="button" class="badge bg-success text-decoration-none" data-bs-toggle="modal" data-bs-target="#tambahLaporan">
                <i data-feather="plus"></i> Tambah Laporan
            </badge> --}}

        </div>
    </div>

</section>

{{-- -------------------------- MODAL TAMBAH LAPORAN KEGIATAN -------------------------- --}}
    <!-- Modal -->
    <div class="modal fade" id="tambahlaporan" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title mx-auto" id="uploadModalLabel">Laporan</h3>
                </button>
              </div>
              <div class="modal-body m-3">
                <form enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="absenFile"><h5>Absen</h5></label><br>
                    <input type="file" class="form-control-file" id="absenFile" style="display: none;">
                    <label class="btn btn-light px-6 btn-outline" for="absenFile"><i data-feather="upload" style="color: gray;"></i> upload absen</label>
                  </div>
                  <div class="form-group mt-4">
                    <label for="dokumentasiFile"><h5>Dokumentasi Kegiatan</h5></label><br>
                    <input type="file" class="form-control-file" id="dokumentasiFile" style="display: none;">
                    <label class="btn btn-light px-6 btn-outline" for="absenFile"><i data-feather="upload" style="color: gray;"></i> upload dokumentasi</label>
                  </div>
                </form>
                <button type="button" class="btn btn-dark mt-5 d-block mx-auto pt-3 pb-3" style="width:85%;">Kirim</button>
              </div>
            </div>
          </div>
      </div>
