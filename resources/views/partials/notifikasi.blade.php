<section id="notifikasi">
    <div class="container mt-5 mb-5">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center bg-white mx-4 mt-4">
                <h5 class="mb-0">Notifikasi</h5>
            </div>
            @foreach ($questions as $question)
            <div class="card-body">
                <a href="{{ route('notification.show_answer', $question->id) }}" style="text-decoration: none; color: inherit;">
                    <div class="card mt-4 mb-4 mx-4" style="background-color: #F0F4F8;">
                        <table style="width: 100%;">
                            <tr>
                                <td class="p-4" style="width: 100px;">
                                    <img src="img/green_notification.png" alt="" style="height: 100px; width: 100px;">
                                </td>
                                <td>
                                    <div class="mt-4 mx-4">
                                        <h6>Admin telah menjawab pertanyaan anda</h6>
                                    </div>
                                    <div class="mx-4">
                                        <p>{{ $question->answer_content }}</p>
                                    </div>
                                    <div class="d-flex justify-content-end px-5 text-muted">
                                        <p>{{ $question->updated_at->format('j F Y') }}</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>

