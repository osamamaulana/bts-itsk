<style>
    .data-row {
        display: grid;
        grid-template-columns: 1fr 3fr 3fr 3fr 2fr 1fr;
        align-items: center;
        border-top: 1px solid #ccc;
        padding-top: 10px;
        padding-bottom: 10px;
    }

    .data-row:first-child {
        border-top: none;
    }

    .data-cell {
        text-align: left;
    }

    .status-badge {
        margin-top: 5px;
    }

    .detail-button {
        margin-top: 5px;
    }
</style>

<section id="riwayatpendaftaran">
    <div class="container mt-5 mb-5">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center bg-white">
                <h5 class="mb-0 ">Riwayat Pendaftaran</h5>
                <span><a href="{{ url('/User/form') }}" class="btn btn-success px-5">Daftar</a></span>
            </div>
            <div class="card m-4">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Sekolah</th>
                            <th scope="col">Provinsi</th>
                            <th scope="col">Tanggal Pengajuan</th>
                            <th scope="col">Status</th>
                            <th scope="col">Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $key => $kegiatan)
                        <tr>
                            <td class="pt-3 pb-3 px-3">{{ $key+1 }}</td>
                            <td class="pt-3 pb-3">{{ $kegiatan->sekolah }}</td>
                            <td class="pt-3 pb-3">{{ $kegiatan->provinsi->provinsi }}</td>
                            <td class="pt-3 pb-3">{{ \Carbon\Carbon::parse($kegiatan->tanggal_kegiatan)->formatLocalized('%A, %e %B %Y') }}</td>
                            <td class="pt-3 pb-3">
                                @if($kegiatan->status_promosi == 'Diterima')
                                <span class="badge bg-success">{{ $kegiatan->status_promosi }}</span>
                                @elseif($kegiatan->status_promosi == 'Diproses')
                                <span class="badge bg-info">{{ $kegiatan->status_promosi }}</span>
                                @elseif($kegiatan->status_promosi == 'Ditolak')
                                <span class="badge bg-danger">{{ $kegiatan->status_promosi }}</span>
                                @endif
                            </td>
                            <td class="pt-3 pb-3">
                                @if($kegiatan->status_promosi == 'Diterima')
                                <a href="{{ route('detailKegiatan', $kegiatan->id) }}" class="btn btn-info detail-button">Lihat</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</section>
