<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('title')</title>

    {{-- BOOTSTRAP CSS  --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">


    {{-- FEATHER ICONS  --}}
    <script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>

    {{-- MULTY SELECT  --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/habibmhamadi/multi-select-tag@3.0.1/dist/css/multi-select-tag.css">
    <script src="https://cdn.jsdelivr.net/gh/habibmhamadi/multi-select-tag@3.0.1/dist/js/multi-select-tag.js"></script>

</head>

<body class="bg-light">

    @yield('registration')

</body>

{{-- BOOTSTRAP JS  --}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

{{-- FEATHER ICONS  --}}
<script src="https://unpkg.com/feather-icons"></script>
<script>
    feather.replace();
    $(document).ready(function() {
        $('#id_provinsi').on('change', function() {
            var id_provinsi = $(this).val();

            if (id_provinsi) {
                $.ajax({
                    url: '/get-sekolah-by-provinsi/' + id_provinsi,
                    type: 'GET',
                    data: {
                        '_token': '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data) {
                            $('#nama_sekolah').empty();
                            $('#nama_sekolah').append('<option value="">Pilih</option>');
                            $.each(data, function(key, sekolah) {
                                $('#nama_sekolah').append(
                                    '<option value="' + sekolah.id + '">' +
                                    sekolah.nama_sekolah + '</option>'
                                );
                            });
                        } else {
                            $('#nama_sekolah').empty();
                        }
                    }
                });
            } else {
                $('#nama_sekolah').empty();
            }
        });
    });
</script>

{{-- MULTY SELECT  --}}
<script>
    new MultiSelectTag('select_users')  // id
</script>

</html>
