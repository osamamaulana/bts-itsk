@extends('login.loginform')

@section('title', 'BTS-ITSK | Change Password')

@section('container')

    <div class="col-12 col-sm-10 col-xl-8 p-4 p-sm-5">
        <h1 class="fs-2">Buat Password Baru</h1>
        <p class="fw-semibold mb-5">
            Buat password baru untuk login ke akun anda!
        </p>
        <form action="{{ route('reset.password.post') }}" method="post">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <input type="hidden" name="email" value="{{ $email }}">

            <label for="password" class="form-label fw-semibold mb-0">Password Baru</label>
            <input type="password" class="form-control mb-3 p-2" id="password" placeholder="Masukkan password baru"
                name="password" required autofocus>
            @if ($errors->has('password'))
                <span class="text-danger">{{ $errors->first('password') }}</span>
            @endif

            <label for="password_confirmation" class="form-label fw-semibold mb-0">Konfirmasi Password Baru</label>
            <input type="password" class="form-control mb-5 p-2" id="password_confirmation"
                placeholder="Masukkan password baru" name="password_confirmation" required autofocus>
            @if ($errors->has('password_confirmation'))
                <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
            @endif

            <button type="submit" class="btn btn-dark w-100 p-2">Reset Password</button>
        </form>
    </div>

@endsection
