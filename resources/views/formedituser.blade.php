<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit User</title>
</head>
<body>
    <h1>Edit User</h1>
    <form action="{{ route('usersmanagement.update', $user->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div>
            <label for="nama">Nama:</label>
            <input type="text" id="nama" name="nama" value="{{ $user->nama }}">
        </div>
        <div>
            <label for="nim">NIM:</label>
            <input type="text" id="nim" name="nim" value="{{ $user->nim }}">
        </div>
        <div>
            <label for="email">Email:</label>
            <input type="email" id="email" name="email" value="{{ $user->email }}">
        </div>
        <div>
            <label for="role">Role:</label>
            <!-- Dropdown menu untuk memilih role -->
            <select name="role">
                <option value="Dosen" {{ $user->role == 'Dosen' ? 'selected' : '' }}>Dosen</option>
                <option value="Mahasiswa" {{ $user->role == 'Mahasiswa' ? 'selected' : '' }}>Mahasiswa</option>
                <option value="Admin" {{ $user->role == 'Admin' ? 'selected' : '' }}>Admin</option>
            </select>
        </div>
        <button type="submit">Update User</button>
        <a href="{{ url()->previous() }}"><button type="button">Cancel</button></a>
    </form>
</body>
</html>
