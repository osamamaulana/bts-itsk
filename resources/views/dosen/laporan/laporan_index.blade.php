<div class="container">
    <h1>Daftar Laporan</h1>
    <table class="table">
        <thead>
            <tr>
                <th>ID Laporan</th>
                <th>ID Kegiatan</th>
                <th>Status Promosi</th>
                <th>Tanggal Laporan</th>
                <th>Aksi</th>
                <th>File Laporan</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($laporan as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->id_kegiatan }}</td>
                    <td>{{ $item->status_promosi }}</td>
                    <td>{{ $item->tanggal_laporan }}</td>
                    <td>
                        <a href="{{ route('dosen.laporan.edit', $item->id) }}" class="btn btn-primary">Edit</a>
                    </td>
                    <td>
                        @foreach ($item->files as $file)
                            <a href="{{ asset('storage/' . $file->dokumen) }}" target="_blank">{{ $file->nama_file }}</a><br>
                        @endforeach
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
