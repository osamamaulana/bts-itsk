<form action="{{ route('dosen.laporan.update', $laporan->id) }}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="status_promosi">Status Promosi</label>
        <select name="status_promosi" id="status_promosi" class="form-control">
            <option value="Diterima" {{ $laporan->status_promosi == 'Diterima' ? 'selected' : '' }}>Diterima</option>
            <option value="Diproses" {{ $laporan->status_promosi == 'Diproses' ? 'selected' : '' }}>Diproses</option>
            <option value="Ditolak" {{ $laporan->status_promosi == 'Ditolak' ? 'selected' : '' }}>Ditolak</option>
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
</form>
