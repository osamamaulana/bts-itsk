<!-- Tampilkan semua data users -->
<h1>Users Management</h1>
    <a href="{{ route('usersmanagement.create') }}"><button type="button">Tambah User</button></a>
<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>NIM</th>
            <th>Email</th>
            <th>Role</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $user->nama }}</td>
            <td>{{ $user->nim }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->role }}</td>
            <td>
                <a href="{{ route('usersmanagement.edit', $user->id) }}">Edit</a>
                <form action="{{ route('usersmanagement.destroy', $user->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Hapus</button>
                </form>                
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
