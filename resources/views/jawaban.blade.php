@extends('layouts.mainform')

@section('title', 'BTS-ITSK | Jawaban Pertanyaan')

@section('registration')
    {{-- NAVBAR  --}}
    @include('partials.navbar')

    {{-- NOTIFIKASI  --}}
    @include('partials.jawaban')

    {{-- FOOTER  --}}
    @include('partials.footer')
@endsection
