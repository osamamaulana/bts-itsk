@extends('admin.dashboard.layouts.main')

@php
    $title = 'Kegiatan';
@endphp

@section('title')
    Dashboard Edit Kegiatan
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection




@section('content')
<div class="col-11 col-sm-10 col-md-9 mx-auto mb-5 p-4 p-sm-5 border"
        style="background-color: rgb(255, 255, 255); margin-top: 125px; border-radius: 10px">
    <form action="{{ route('admin.kegiatan.update', $kegiatan->id) }}" method="POST">
    @csrf
    @method('PUT')
    <h1 class="fs-5 mb-5 pb-2 border-bottom border-2">Edit Kegiatan</h1>
      <div class="mb-4">
        <label for="status_promosi" class="form-label fw-semibold">Status Promosi</label>
        <select name="status_promosi" id="status_promosi" class="form-select p-2">
            <option value="Diterima">Diterima</option>
            <option value="Diproses" selected>Diproses</option>
            <option value="Ditolak">Ditolak</option>
        </select>
    </div>
            

    {{-- <div>
        <label for="catatan_promosi">Catatan Promosi:</label>
        <textarea name="catatan_promosi" id="catatan_promosi" rows="4">{{ $kegiatan->catatan_promosi }}</textarea>
    </div> --}}

    <button type="submit" class="btn btn-dark d-block mx-auto mt-5 px-5 py-2">simpan</button>
    </form>
</div>
@endsection