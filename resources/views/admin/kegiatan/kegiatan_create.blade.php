@extends('admin.dashboard.layouts.main')

@php
    $title = 'Kegiatan';
@endphp

@section('title')
    Dashboard Tambah Kegiatan
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 col-sm-10 col-md-9 mx-auto mb-5 p-4 p-sm-5 border"
        style="background-color: rgb(255, 255, 255); margin-top: 125px; border-radius: 10px">
        <form action="{{ route('admin.kegiatan.store') }}" method="POST">
            @csrf
            <h1 class="fs-5 mb-5 pb-2 border-bottom border-2">Tambah Data Kegiatan</h1>

            {{-- <div class="mb-4">
                <label for="mahasiswa" class="form-label fw-semibold">Nama Mahasiswa</label>
                <input type="text" class="form-control p-2" id="mahasiswa" name="mahasiswa" placeholder="Masukkan Nama Mahasiswa" required>
            </div>

            <div class="mb-4">
                <label for="dosen" class="form-label fw-semibold">Nama Dosen</label>
                <input type="text" class="form-control p-2" id="dosen" name="dosen" placeholder="Masukkan Nama Dosen" required>
            </div> --}}

            <div class="mb-4">
                <label for="id_provinsi" class="form-label fw-semibold">Provinsi</label>
                <select name="id_provinsi" id="id_provinsi" class="form-select p-2">
                    @foreach ($provinsi as $provinsi)
                        <option value="{{ $provinsi->id }}">{{ $provinsi->provinsi }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-4">
                <label for="tanggal_kegiatan" class="form-label fw-semibold">Tanggal Kegiatan</label>
                <input type="datetime-local" class="form-control p-2" id="tanggal_kegiatan" name="tanggal_kegiatan" required>
            </div>

            <div class="mb-4">
                <label for="sekolah" class="form-label fw-semibold">Sekolah</label>
                <input type="text" class="form-control p-2" id="sekolah" name="sekolah" placeholder="Masukkan Nama Sekolah" required>
            </div>

            <div class="mb-4">
                <label for="status_promosi" class="form-label fw-semibold">Status Promosi</label>
                <select name="status_promosi" id="status_promosi" class="form-select p-2">
                    <option value="Diterima">Diterima</option>
                    <option value="Diproses" selected>Diproses</option>
                    <option value="Ditolak">Ditolak</option>
                </select>
            </div>
            <button type="submit" class="btn btn-dark d-block mx-auto mt-5 px-5 py-2">simpan</button>
        </form>
    </div>
@endsection
