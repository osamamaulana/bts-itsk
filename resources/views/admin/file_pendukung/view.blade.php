@extends('admin.dashboard.layouts.main')

@section('title', 'Daftar File Pendukung')

@php
    $title = 'Daftar File Pendukung';
@endphp

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="container">
        <div class="col-11 mx-auto mb-5 border overflow-hidden" style="background-color: rgb(255, 255, 255); font-size: 12.5px; margin-top: 125px; border-radius: 10px;">
           <div class="d-flex justify-content-between align-items-center p-4">
                <h1 class="fs-5 mb-0">File Pendukung</h1>
                <a href="{{ route('Admin.file_pendukung.create') }}" class="btn" style="border-radius: 20px; background-color: #4CAF50; color: white; padding: 3px 6px;">
                    <span>+</span>
                    <span>Tambah Data</span>
                </a>
            </div>
            <div class="d-flex justify-content-between align-items-center text-secondary px-4">
                <form action="#" method="post" class="">
                    <label for="show">Show</label>
                    <input type="number" name="show" id="show" value="10" class="border border-2"
                        style="width: 75px; border-radius: 5px;">
                    <span>entries</span>
                </form>
                <form action="#" method="post" class="float-right">
                    <label for="search">Search :</label>
                    <input type="text" name="search" id="search" class="border border-2 px-1" style="width: 200px; border-radius: 5px;">
                </form>
            </div>
            @if(session('success'))
                <div class="mt-3 alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif
            <div class="table-responsive mt-3">
                <table class="table table-hover">
                    <thead class="table-light border-top border-bottom">
                        <tr>
                            <th scope="col" class="text-secondary px-4">ID</th>
                            <th scope="col" class="text-secondary px-4" style="white-space: nowrap;">Nama File</th>
                            <th scope="col" class="text-secondary px-4">Jenis</th>
                            <th scope="col" class="text-center text-secondary px-4">File</th>
                            <th scope="col" class="text-secondary px-4">Status</th>
                            <th scope="col" class="text-secondary px-4">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($file_pendukung as $key => $file)
                            <tr>
                                <td scope="col" class="px-4 py-3">{{ $key + 1 }}</td>
                                <td scope="col" class="px-4 py-3">{{ $file->nama }}</td>
                                <td scope="col" class="px-4 py-3">{{ $file->jenis }}</td>
                                <td scope="col" class="px-4 py-3">{{ $file->url }}</td>
                                <td scope="col" class="px-4 py-3">
                                    <form action="{{ route('Admin.file_pendukung.update_status', $file->id) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn toggle-button  btn-sm" style="border-radius: 4px; background-color: {{ $file->is_active ? '#53E62F' : '#C20C0C' }}; color: white;">
                                            {{ $file->is_active ? 'AKTIF' : 'TIDAK AKTIF' }}
                                        </button>
                                    </form>
                                </td>
                                <td scope="col" class="px-4 py-3">
                                    <form action="{{ route('Admin.file_pendukung.delete', $file->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn text-secondary fs-4 mx-2 btn-sm">
                                            <i class="bi bi-trash3"></i> 
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-flex justify-content-between text-secondary p-4">
                <div>Showing <span>1</span> to <span>10</span> of <span>50</span> entries</div>
                <nav>
                    <ul class="pagination">
                        <li class="page-item disabled">
                            <a class="page-link text-secondary">
                                <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                            </a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link text-secondary" href="#">2</a></li>
                        <li class="page-item"><a class="page-link text-secondary" href="#">3</a></li>
                        <li class="page-item"><a class="page-link text-secondary" href="#">4</a></li>
                        <li class="page-item"><a class="page-link text-secondary" href="#">5</a></li>
                        <li class="page-item">
                            <a class="page-link text-secondary" href="#">
                                Next<i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div> 
        </div>
    </div>
@endsection
