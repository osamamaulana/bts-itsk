<form action="{{ route('admin.laporan.update', $laporan->id) }}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="status_promosi">Status Promosi</label>
        <select name="status_promosi" id="status_promosi" class="form-control">
            <option value="Diterima" {{ $laporan->status_promosi == 'Diterima' ? 'selected' : '' }}>Diterima</option>
            <option value="Diproses" {{ $laporan->status_promosi == 'Diproses' ? 'selected' : '' }}>Diproses</option>
            <option value="Ditolak" {{ $laporan->status_promosi == 'Ditolak' ? 'selected' : '' }}>Ditolak</option>
        </select>
    </div>

    <div class="form-group">
        <label for="tanggal_laporan">Tanggal Laporan</label>
        <input type="datetime-local" name="tanggal_laporan" id="tanggal_laporan" class="form-control"
            value="{{ $laporan->tanggal_laporan }}">
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
</form>