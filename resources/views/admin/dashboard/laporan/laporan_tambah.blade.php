@extends('admin.dashboard.layouts.main')

@php
    $title = 'Laporan';
@endphp

@section('title')
    Dashboard Tambah Laporan
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 col-sm-10 col-md-9 mx-auto mb-5 p-4 p-sm-5 border"
        style="background-color: rgb(255, 255, 255); margin-top: 125px; border-radius: 10px">
        <form action="#" method="post">
            <h1 class="fs-5 mb-5 pb-2 border-bottom border-2">Tambah Data Laporan</h1>

            <div class="mb-4">
                <label for="nama_ketua" class="form-label fw-semibold">Nama Ketua</label>
                <input type="text" class="form-control p-2" id="nama_ketua" name="nama_ketua"
                    placeholder="Masukkan Nama Ketua" required>
            </div>

            <div class="mb-4">
                <label for="nama_sekolah" class="form-label fw-semibold">Nama Sekolah</label>
                <input type="text" class="form-control p-2" id="nama_sekolah" name="nama_sekolah"
                    placeholder="Masukkan Nama Sekolah" required>
            </div>

            <div class="mb-4">
                <label for="file" class="form-label fw-semibold">File</label>
                <input type="file" class="form-control p-2" id="file" name="file" multiple required>
            </div>

            <div class="mb-4">
                <label for="tanggal_laporan" class="form-label fw-semibold">Tanggal Laporan</label>
                <input type="date" class="form-control p-2" id="tanggal_laporan" name="tanggal_laporan"
                    placeholder="Masukkan Tanggal Laporan" required>
            </div>

            <button type="submit" class="btn btn-dark d-block mx-auto mt-5 px-5 py-2">Simpan</button>
        </form>
    </div>
@endsection