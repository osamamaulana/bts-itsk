@extends('admin.dashboard.layouts.main')

@php
    $title = 'Laporan';
@endphp

@section('title')
    Dashboard Laporan
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 mx-auto mb-5 border overflow-hidden"
        style="background-color: rgb(255, 255, 255); font-size: 13px; margin-top: 125px; border-radius: 10px">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center p-4">
            <h1 class="fs-5 mb-3 mb-sm-0">Data Laporan</h1>
            <a href="{{ url('/dashboard-tambah-laporan') }}" class="btn btn-success align-self-end" style="border-radius: 25px">
                <span>+</span>
                <span>Tambah Data</span>
            </a>
        </div>
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center text-secondary px-4">
            <form action="#" method="post" class="mb-3 mb-sm-0">
                <label for="show">Show</label>
                <input type="number" name="show" id="show" value="10" class="border border-2"
                    style="width: 75px; border-radius: 5px;">
                <span>entries</span>
            </form>
            <form action="#" method="post" class="">
                <label for="search">Search :</label>
                <input type="text" name="search" id="search" class="border border-2 px-1"
                    style="width: 200px; border-radius: 5px;">
            </form>
        </div>

        {{-- Table --}}
        <div class="table-responsive mt-4">
            <table class="table table-hover">
                <thead class="table-light border-top border-bottom">
                    <tr>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">ID</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">NAMA KETUA</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">NAMA SEKOLAH</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">FILE</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">TANGGAL LAPORAN</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">ACTION</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td class="text-secondary text-center px-3 text-nowrap">001</td>
                        <td class="px-3 text-nowrap">Budi Santoso</td>
                        <td class="px-3 text-nowrap">SMA Negeri 1 Jakarta</td>
                        <td class="px-3 text-center text-nowrap">
                            <form action="#" method="post" class="">
                                <button type="submit" name="lihat" class="btn btn-primary" style="font-size: 13px; border-radius: 25px">
                                    <span style="margin-right: 5px"><i class="bi bi-eye"></i></span>
                                    <span>Lihat</span>
                                </button>
                            </form>
                        </td>
                        <td class="px-3 text-nowrap">30 Juni 2024</td>
                        <td class="text-center px-3 text-nowrap">
                            <form action="#" method="post" class="">
                                {{-- Tombol Disetujui --}}
                                <button type="submit" name="disetujui" class="btn btn-success mx-1" style="font-size: 13px">
                                    <span>Disetujui</span>
                                    <span><i class="bi bi-check2-square"></i></span>
                                </button>
                                {{-- Tombol Revisi --}}
                                <button type="submit" name="revisi" class="btn btn-warning mx-1" style="font-size: 13px">
                                    <span><i class="bi bi-pencil-square"></i></span>
                                    <span>Revisi</span>
                                </button>
                            </form>
                        </td>
                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td class="text-secondary text-center px-3 text-nowrap">002</td>
                        <td class="px-3 text-nowrap">Budi Santoso</td>
                        <td class="px-3 text-nowrap">SMA Negeri 1 Jakarta</td>
                        <td class="px-3 text-center text-nowrap">
                            <button type="submit" name="lihat" class="btn btn-primary" style="font-size: 13px; border-radius: 25px">
                                <span style="margin-right: 5px"><i class="bi bi-eye"></i></span>
                                <span>Lihat</span>
                            </button>
                        </td>
                        <td class="px-3 text-nowrap">30 Juni 2024</td>
                        <td class="text-center px-3 text-nowrap">
                            <form action="#" method="post" class="">
                                {{-- Tombol Disetujui --}}
                                <button type="submit" name="disetujui" class="btn btn-success mx-1" style="font-size: 13px">
                                    <span>Disetujui</span>
                                    <span><i class="bi bi-check2-square"></i></span>
                                </button>
                                {{-- Tombol Revisi --}}
                                <button type="submit" name="revisi" class="btn btn-warning mx-1" style="font-size: 13px">
                                    <span><i class="bi bi-pencil-square"></i></span>
                                    <span>Revisi</span>
                                </button>
                            </form>
                        </td>
                    </tr>
                </tbody>

                <tbody>
                    <tr>
                        <td class="text-secondary text-center px-3 text-nowrap">003</td>
                        <td class="px-3 text-nowrap">Budi Santoso</td>
                        <td class="px-3 text-nowrap">SMA Negeri 1 Jakarta</td>
                        <td class="px-3 text-center text-nowrap">
                            <button type="submit" name="lihat" class="btn btn-primary" style="font-size: 13px; border-radius: 25px">
                                <span style="margin-right: 5px"><i class="bi bi-eye"></i></span>
                                <span>Lihat</span>
                            </button>
                        </td>
                        <td class="px-3 text-nowrap">30 Juni 2024</td>
                        <td class="text-center px-3 text-nowrap">
                            <form action="#" method="post" class="">
                                {{-- Tombol Disetujui --}}
                                <button type="submit" name="disetujui" class="btn btn-success mx-1" style="font-size: 13px">
                                    <span>Disetujui</span>
                                    <span><i class="bi bi-check2-square"></i></span>
                                </button>
                                {{-- Tombol Revisi --}}
                                <button type="submit" name="revisi" class="btn btn-warning mx-1" style="font-size: 13px">
                                    <span><i class="bi bi-pencil-square"></i></span>
                                    <span>Revisi</span>
                                </button>
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        {{-- Table End --}}

        <div class="d-flex flex-column flex-md-row justify-content-between align-items-center text-secondary p-4">
            <div>Showing <span>1</span> to <span>10</span> of <span>50</span> entries</div>
            <nav class="mt-5 mt-md-0">
                <ul class="pagination mb-0">
                    <li class="page-item disabled">
                        <a class="page-link text-secondary">
                            <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                        </a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link text-secondary" href="#">2</a></li>
                    <li class="page-item"><a class="page-link text-secondary" href="#">3</a></li>
                    <li class="page-item"><a class="page-link text-secondary" href="#">4</a></li>
                    <li class="page-item"><a class="page-link text-secondary" href="#">5</a></li>
                    <li class="page-item">
                        <a class="page-link text-secondary" href="#">
                            Next<i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
@endsection