@extends('dashboard.layouts.main')

@section('title', 'Dashboard')

@section('sidebar')
    @include('dashboard.partials.sidebar')
@endsection

@section('content')
    @include('dashboard.partials.navbar')
@endsection
