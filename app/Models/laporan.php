<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class laporan extends Model
{
    use HasFactory;

    protected $fillable = ['id_kegiatan', 'status_promosi', 'tanggal_laporan'];
    public function files()
    {
        return $this->belongsToMany(file_laporan::class, 'pivot_laporan_files', 'id_lp', 'id_fl');
    }
}
