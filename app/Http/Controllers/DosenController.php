<?php

namespace App\Http\Controllers;

use App\Models\kegiatan;
use App\Models\laporan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DosenController extends Controller
{
    //view
    public function view_kegiatan()
    {
        $kgt['kegiatan'] = kegiatan::all();
        return view('dosen.kegiatanView', $kgt);
    }
    public function kegiatan_edit($id)
    {
        $editData = kegiatan::find($id);
        return view('dosen.edit', compact('editData'));
    }
    //edit_kegiatan
    public function updatekegiatan(Request $request, $id)
    {
        $request->validate([
            'status_promosi' => 'required|in:Diterima,Diproses,Ditolak',
        ]);

        $kegiatan = kegiatan::findOrFail($id);
        $kegiatan->status_promosi = $request->status_promosi;
        $kegiatan->save();

        return redirect()->route('dosen.view')->with('success', 'Status promosi kegiatan berhasil diperbarui');
    }
    //delete_kegiatan
    public function deletekegiatan($id)
    {
        $kegiatan = kegiatan::find($id);
        $kegiatan->delete();

        return redirect()->route('dosen.view')->with('success', 'Status promosi kegiatan berhasil dihapus');
    }

    // LAPORAN
    public function indexLaporan()
    {
        $laporan = laporan::all();
        return view("dosen.laporan.laporan_index", compact("laporan"));
    }

    public function editLaporan($id)
    {
        $laporan = laporan::findOrFail($id);
        return view("dosen.laporan.laporan_edit", compact("laporan"));
    }

    public function updateLaporan(Request $request, $id)
    {
        $request->validate([
            "status_promosi" => "required|in:Diterima,Diproses,Ditolak",
        ]);

        $laporan = Laporan::findOrFail($id);
        $laporan->update($request->all());

        return redirect()->route("dosen.laporan.index")->with("success", "Laporan berhasil diperbarui.");
    }

    public function downloadLaporan($id)
    {
        $laporan = laporan::findOrFail($id);
        if (!$laporan) {
            abort(404);
        }

        $filePaths = [];
        foreach ($laporan->files as $file) {
            $filePaths[] = Storage::path($file->dokumen);
        }

        foreach ($filePaths as $filePath) {
            readfile($filePath);
        }
    }
}
