<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\UserManagement;

class UserManagementController extends Controller
{
    public function index()
    {
        $users = UserManagement::select('id', 'nama', 'nim', 'email', 'role')->get();
        
        return view('usersmanagement', compact('users'));
    }    

    public function create()
    {
        return view('formtambahuser');
    }

    public function store(Request $request)
    {
    
        $request->merge(['password' => Hash::make($request->password)]);
    
        UserManagement::create($request->all());

        return redirect()->route('usersmanagement.index')->with('success', 'User berhasil ditambahkan!');
    }    
    
    public function edit($id)
    {
        $user = UserManagement::findOrFail($id);
        return view('formedituser', compact('user'));
    }
    
    public function update(Request $request, $id)
    {
        $user = UserManagement::findOrFail($id);
        $user->update($request->only(['nama', 'nim', 'email', 'role']));
        return redirect()->route('usersmanagement.index')->with('success', 'Informasi pengguna berhasil diperbarui!');
    }
    
    public function destroy(UserManagement $user)
    {
        $user->delete();

        return redirect()->route('usersmanagement.index')->with('success', 'User berhasil dihapus!');
    }
    
}

