<?php

namespace App\Http\Controllers;

use App\Models\FilePendukung;
use App\Models\Question;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    public function dashboard()
    {
        return view('admin.dashboard.admin.admin_index');
    }
    // FILE PENDUKUNG BAHAN PRESENTASI 
    public function View_File_Pendukung()
    {
        $file_pendukung = FilePendukung::all();
        return view('admin.file_pendukung.view', compact('file_pendukung'));
    }
    public function create_file_pendukung()
    {
        return view('admin.file_pendukung.create');
    }
    public function store_file(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'jenis' => 'required',
            'file' => 'required|file',
        ]);

        $file = $request->file('file');
        $nama_file = Str::random(10) . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs('public/file_presentasi', $nama_file);

        $url = asset(Storage::url($path));

        FilePendukung::create([
            'nama' => $request->nama,
            'jenis' => $request->jenis,
            'file' => $nama_file,
            'url' => $url,
            'is_active' => true,
        ]);
        return redirect()->route('Admin.file_pendukung.view')->with('success', 'File pendukung berhasil ditambahkan.');
    }
    public function update_file_status($id)
    {
        $file_pendukung = FilePendukung::findOrFail($id);
        $file_pendukung->is_active = !$file_pendukung->is_active;
        $file_pendukung->save();

        return redirect()->route('Admin.file_pendukung.view')->with('success', 'Status file pendukung berhasil diubah.');
    }

    // Fungsi untuk menghapus file pendukung
    public function delete_file($id)
    {
        $file_pendukung = FilePendukung::findOrFail($id);
        Storage::delete('public/file_presentasi/' . $file_pendukung->file);
        $file_pendukung->delete();

        return redirect()->route('Admin.file_pendukung.view')->with('success', 'File pendukung berhasil dihapus.');
    }

    public function pesanMasuk()
    {
        $questions = Question::where('is_answered', false)->get();

        return view('admin.pesan_masuk', compact('questions'));
    }
}
