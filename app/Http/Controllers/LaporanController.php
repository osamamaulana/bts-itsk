<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\laporan;

class LaporanController extends Controller
{
    public function index()
    {
        $laporan = laporan::all();
        return view("admin.laporan.laporan_index", compact("laporan"));
    }

    public function editForm($id)
    {
        $laporan = Laporan::findOrFail($id);
        return view("admin.laporan.laporan_edit", compact("laporan"));
    }

    public function updateLaporan(Request $request, $id)
    {
        $request->validate([
            "status_promosi" => "required|in:Diterima,Diproses,Ditolak",
            "tanggal_laporan" => "required|date|date_format:Y-m-d\TH:i:s",
        ]);

        $laporan = Laporan::findOrFail($id);
        $laporan->update($request->all());

        return redirect()->route("admin.laporan.index")->with("success", "Laporan berhasil diperbarui.");
    }
}
