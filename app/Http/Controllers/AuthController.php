<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    function loginPage()
    {
        return view("signin"); // page login
    }

    function prosesLogin(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            if (Auth::user()->role == 'mahasiswa') { // untuk mahasiswa
                return redirect('/');
            } elseif (Auth::user()->role == 'dosen') { // untuk dosen
                return redirect(''); // page user dosen

            } else { // untuk admin
                return redirect()->route('admin.dashboard'); // page user admin
            }
        } else {
            return redirect('signin') // page login
                ->withErrors(["Login Gagal"])->withInput();
        }
    }

    function registerPage()
    {
        return view("signup"); // page register
    }

    public function processRegister(Request $request)
    {
        $request->validate([
            "nama"              => "required",
            "nim"               => "min:12|max:12|unique:users",
            "email"             => "required|unique:users",
            "password"          => "required|min:6",
            "prodi"             => "required|in:TRPL,TRK,BD",
        ]);

        $validProdiOptions = ['TRPL', 'TRK', 'BD'];
        if (!in_array($request->prodi, $validProdiOptions)) {
            return redirect()->back()->withErrors(['Prodi yang dipilih tidak valid.'])->withInput();
        }

        $data = $request->all();

        // dd($data);

        $data['password'] = Hash::make($data['password']);
        $data['role'] = 'mahasiswa'; // Mahasiswa

        $user = User::create($data);

        event(new Registered($user));

        return redirect()->route('login'); // page setelah regis
    }

    public function logout() // need button logout di tampilan
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
