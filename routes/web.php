<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\DosenController;
use App\Http\Controllers\KontenController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserManagementController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\KegiatanController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\PemberitahuanController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    if (Auth::check()) {
        return view('index');
    } else {
        return view('welcome');
    }
});
//
Route::get('/buat/{id}', [KontenController::class, 'proses_sertifikat'])->name('buat');
// LOG IN
Route::get('/signin', [AuthController::class, 'loginPage'])->name('login');
Route::post('/signin', [AuthController::class, 'prosesLogin'])->name('login.submit');

// REGISTER
Route::get('/signup', [AuthController::class, 'registerPage'])->name('register');
Route::post('/signup', [AuthController::class, 'processRegister'])->name('register.submit');

// LOG OUT
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

// FORGOT-PASSWORD
Route::get('forget-password', [ForgotPasswordController::class, 'showForgetPasswordForm'])->name('forget.password.get');
Route::post('forget-password', [ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password.post');
Route::get('reset-password/{token}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
Route::post('reset-password', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');


// CHANGE PASSWORD
Route::get('/change-password', function () {
    return view('changepassword');
});
// -----------------------------------------

// Route::get('/status', function () {
//     return view('statuspengajuan');
// });

// Route::get('/formpengajuan', function () {
//     return view('formpengajuan');
// });

// Route::get('/detailkegiatan', function () {
//     return view('detailkegiatan');
// });


Route::get('/profile', function () {
    return view('userprofile');
});


// HAK AKSES UNTUK ROLE USER UNTUK MENCOBA http://127.0.0.1:8000/User/view
Route::prefix('/User')->middleware(['auth', 'check-access:mahasiswa'])->group(function () {
    Route::get('/view', [KontenController::class, 'kegiatan_view'])->name('view');
    Route::get('/form', [KontenController::class, 'kegiatanUser_Add_View'])->name('formpengajuan');
    Route::post('/store/kegiatan', [KontenController::class, 'KegiatanUser_Store'])->name('kegiatan.store');
    Route::get('/detail/kegiatan/{id}', [KontenController::class, 'detailKegiatan'])->name('detailKegiatan');
    Route::get('/laporan/create/{id_kegiatan}', [KontenController::class, 'tambah_laporan'])->name('laporan.form');
    Route::post('/laporan/store', [KontenController::class, 'Proses_laporan'])->name('laporan.store');
    Route::get('/download-ppt', [KontenController::class, 'downloadPPT'])->name('downloadPPT');

    Route::get('/profile', [ProfileController::class, 'index'])->name('user.profile');
    Route::post('/profile', [ProfileController::class, 'store'])->name('user.profile.store');
    Route::post('/profile/change-password', [ProfileController::class, 'changePassword'])->name('profile.change.password');

    Route::post('/submit-question', [QuestionController::class, 'submitQuestion'])->name('submit.question');
    Route::get('/notifikasi', [PemberitahuanController::class, 'index'])->name('notification.index');
    Route::get('/notifikasi/jawaban/{questionId}', [PemberitahuanController::class, 'showAnswer'])->name('notification.show_answer');
});
//HAK AKSES UNTUK ROLE ADMIN 
Route::prefix('/admin')->middleware(['auth', 'check-access:admin'])->group(function () {
    // ROUTE DASHBOARD
    Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('admin.dashboard');
    Route::get('/form', [AdminController::class, 'view_form_bahan_presentasi'])->name('form_file');
    Route::post('/store/file', [AdminController::class, 'store_bahan_presentasi'])->name('file.store');
    Route::get('/usersmanagement', [UserManagementController::class, 'index'])->name('usersmanagement.index');
    Route::get('/usersmanagement/create', [UserManagementController::class, 'create'])->name('usersmanagement.create');
    Route::post('/usersmanagement', [UserManagementController::class, 'store'])->name('usersmanagement.store');
    Route::get('/usersmanagement/{id}/edit', [UserManagementController::class, 'edit'])->name('usersmanagement.edit');
    Route::put('/usersmanagement/{id}', [UserManagementController::class, 'update'])->name('usersmanagement.update');
    Route::delete('/usersmanagement/{user}', [UserManagementController::class, 'destroy'])->name('usersmanagement.destroy');

    Route::get('/dashboard/pesan-masuk', [AdminController::class, 'pesanMasuk'])->name('dashboard.pesan-masuk');
    Route::post('/kirim-jawaban/{question}', [QuestionController::class, 'kirimJawaban'])->name('kirim.jawaban');

    //FILE PENDUKUNG
    Route::get('/file_pendukung', [AdminController::class, 'View_File_Pendukung'])->name('Admin.file_pendukung.view');
    Route::get('/file_pendukung/create', [AdminController::class, 'create_file_pendukung'])->name('Admin.file_pendukung.create');
    Route::post('/file_pendukung', [AdminController::class, 'store_file'])->name('file_pendukung.store');
    Route::post('/file-pendukung/update-status/{id}', [AdminController::class, 'update_file_status'])->name('Admin.file_pendukung.update_status');
    Route::delete('/file-pendukung/delete/{id}', [AdminController::class, 'delete_file'])->name('Admin.file_pendukung.delete');
});

Route::prefix('/admin/dashboard')->middleware(['auth', 'check-access:admin'])->group(function () {
    Route::get('/profile', [ProfileController::class, 'index'])->name('admin.profile');
    Route::post('/profile', [ProfileController::class, 'store'])->name('admin.profile.store');
    Route::post('/profile/change-password', [ProfileController::class, 'changePassword'])->name('admin.profile.change.password');

    Route::get('/kegiatan', [KegiatanController::class, 'indexForm'])->name('admin.kegiatan.index');

    // CREATE HANYA UNTUK UJI COBA CREATE, AGAR TIDAK PERLU PINDAH-PINDAH USER
    Route::get('/kegiatan/create', [KegiatanController::class, 'createForm'])->name('admin.kegiatan.create');
    Route::post('/kegiatan/store', [KegiatanController::class, 'storeKegiatan'])->name('admin.kegiatan.store');

    // YANG DIPAKAI
    Route::get('/kegiatan/{kegiatan}/edit', [KegiatanController::class, 'editForm'])->name('admin.kegiatan.edit');
    Route::put('/kegiatan/{kegiatan}', [KegiatanController::class, 'updateKegiatan'])->name('admin.kegiatan.update');
    Route::delete('/kegiatan/{kegiatan}', [KegiatanController::class, 'destroyKegiatan'])->name('admin.kegiatan.destroy');

    // LAPORAN
    Route::get('/laporan', [LaporanController::class, 'index'])->name('admin.laporan.index');
    Route::get('/laporan/{id}/edit', [LaporanController::class, 'editForm'])->name('admin.laporan.edit');
    Route::put('/laporan/{id}', [LaporanController::class, 'updateLaporan'])->name('admin.laporan.update');
});


Route::prefix('/dosen')->middleware(['auth', 'check-access:dosen'])->group(function () {
    // PROFILE
    Route::get('/profile', [ProfileController::class, 'index'])->name('dosen.profile');
    Route::post('/profile', [ProfileController::class, 'store'])->name('dosen.profile.store');
    Route::post('/profile/change-password', [ProfileController::class, 'changePassword'])->name('dosen.profile.change.password');

    // KEGIATAN
    Route::get('/view/kegiatan', [DosenController::class, 'view_kegiatan'])->name('dosen.view');
    Route::get('/kegiatan/edit/{id}', [DosenController::class, 'kegiatan_edit'])->name('dosen.Edit');
    Route::post('/kegiatan/update/{id}', [DosenController::class, 'updatekegiatan'])->name('kegiatan.Edit');
    Route::get('/kegiatan/delete/{id}', [DosenController::class, 'deletekegiatan'])->name('kegiatan.delete');

    // LAPORAN
    Route::get('/laporan', [DosenController::class, 'indexLaporan'])->name('dosen.laporan.index');
    Route::get('/laporan/edit/{id}', [DosenController::class, 'editLaporan'])->name('dosen.laporan.edit');
    Route::put('/laporan/{id}', [DosenController::class, 'updateLaporan'])->name('dosen.laporan.update');
    Route::get('/laporan/download-file/{id}', [DosenController::class, 'downloadLaporan'])->name('dosen.laporan.download');
});



// ======================== ADMIN DASHBOARD  ========================
// Route::view('/dashboard', 'dashboard.dashboard');

// ======================== ROUTE SEMENTARA =========================

// DASHBOARD ADMIN
Route::get('/dashboard-admin', function () {
    return view('admin.dashboard.admin.admin_index');
});

// DASHBOARD KEGIATAN
Route::get('/dashboard-kegiatan', function () {
    return view('admin.dashboard.kegiatan.kegiatan_index');
});

// DASHBOARD STATUS KEGIATAN
Route::get('/dashboard-status-kegiatan', function () {
    return view('admin.dashboard.kegiatan.kegiatan_status');
});

// DASHBOARD TAMBAH KEGIATAN
Route::get('/dashboard-tambah-kegiatan', function () {
    return view('admin.dashboard.kegiatan.kegiatan_tambah');
});

// DASHBOARD EDIT KEGIATAN
Route::get('/dashboard-edit-kegiatan', function () {
    return view('admin.dashboard.kegiatan.kegiatan_edit');
});

// DASHBOARD LAPORAN
Route::get('/dashboard-laporan', function () {
    return view('admin.dashboard.laporan.laporan_index');
});

// DASHBOARD TAMBAH LAPORAN
Route::get('/dashboard-tambah-laporan', function () {
    return view('admin.dashboard.laporan.laporan_tambah');
});

// ==================================================================

// LAPORAN
Route::get('/laporan', function () {
    return view('laporan');
});

// ======================== ROUTE SEMENTARA =========================



// test sertifikat


