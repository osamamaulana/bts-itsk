<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('file_pendukung', function (Blueprint $table) {
            $table->id();
            $table->string('nama'); //nama file untuk ditampilkan
            $table->string('jenis'); // jenis file : bahan presentasi, absensi, dst
            $table->string('file'); // nama file yang diupload
            $table->string('url'); // url file
            $table->boolean('is_active')->default(true); // status aktif atau tidak
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('file_pendukung');
    }
};
